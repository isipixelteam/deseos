import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ListaItem } from '../../app/classes/index';
import { ListaDeDeseosService } from '../../app/services/lista-deseos.service';

@Component({
	selector: 'app-detalle',
	templateUrl: './detalle.component.html'
})
export class DetalleComponent implements OnInit {

	idx: number;
	lista: any;

	constructor(
		public navCtrl: NavController,
		public alertCtrl: AlertController,
		public navParams: NavParams,
		public _listaDeseos: ListaDeDeseosService)
	{

		this.idx = this.navParams.get("idx");
		this.lista = this.navParams.get("lista");

	}

	ngOnInit(): void { }

	actualizar( item: ListaItem ){

		item.completado = !item.completado;

		let todosMarcados = true;

		for( const item of this.lista.items ) {
			if( !item.completado ){
				todosMarcados = false;
				break;
			}
		}

		this.lista.terminado = todosMarcados;

		this._listaDeseos.actualizarData();
	}

	borrarLista(){
		let confirm = this.alertCtrl.create({
			title: 'Borrar lista',
			message: '¿Estás seguro que querés borrar la lista? Una vez que la borres, fuiste!',
			buttons: ['Cancelar',
				{
					text: 'Aceptar',
					handler: () => {
						this._listaDeseos.borrarLista( this.idx );
						this.navCtrl.pop();
					}
				}
			]
		});
		confirm.present();
	}
}
